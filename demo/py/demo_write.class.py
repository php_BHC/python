__author__ = 'root'
# -*- coding: utf-8 -*-

import time
import os


def writeFun(filename, string):
    file = open(filename, "a")
    if (file and string):
        file.write(string)
    file.close()

#function sleep 10m run
def sleepRunFun():
    time.sleep(2)
    filename = getFileName()
    string = getString()
    writeFun(filename, string)


def forFun(line):
    number = int(line)
    for i in range(number):
        sleepRunFun()


def getString():
    os_name = os.name
    os_cwd = os.getcwd()
    os_env = os.getenv("os")
    os_time = time.strftime('%Y-%m-%d %H-%M-%d')
    string = os_time + '|' + os_name + '|' + os_cwd + '|' + os_cwd + "| \n"
    return string


def getFileName():
    name = time.strftime('%Y-%m-%d.%H') + '.log'
    filename = '../runtime/demo_' + name
    return filename


line = raw_input()
forFun(line)
