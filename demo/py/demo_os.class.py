__author__ = 'Administrator'
# -*- coding: utf-8 -*-
import sys
import os

#1.os.name
def nameFun():
    print os.name

#2.os.getcwd()
def getCwdFun():
    print os.getcwd()

#3.os.getenv
def getEnvFun():
    print os.getenv("OS")

#4.os.putenv
#def putEnvFun():
#5.os.listdir()

def listDirFun(path):
    print os.listdir(path)

#5.removeFun('d:/test.php.bak')
def removeFun(file):
    os.remove(file)
    print  "allready removed" + file

#6.os.system()
def sysTemFun(str_shell):
    os.system(str_shell)

#getCwdFun()
#nameFun()
#getEnvFun()
#listDirFun('d:/')
sysTemFun('pwd')