__author__ = 'root'
# -*- coding: utf-8 -*-

#readline()
def readLineFun(filename):
    file = open(filename,'r')
    if (file):
        print file.readline()
    file.close()

#readlines()
def readLinesFun(filename):
    file = open(filename,'r')
    if(file):
        for line in file.readlines():
            print line
    file.close()

#read()
def readFun(filename,size):
    file = open(filename,'r')
    if(file):
        print file.read(size)
    file.close()

path = '../runtime/demo_log.log'
readLineFun('../runtime/demo_log.log')
#readFun(path,100)