__author__ = 'root'
# -*- coding: utf-8 -*-

import MySQLdb as mdb
import mysql_config_class as db

con = None

try:
    con = mdb.connect('localhost', 'root', 'root', 'test')
    cur = con.cursor()
    cur.execute('SELECT VERSION()')
    data = cur.fetchone()
    #print "Database version : %s " % data
finally:
    if con:
        con.close()

print(db.curFun('select * from user limit 1'))
