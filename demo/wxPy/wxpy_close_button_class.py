__author__ = 'root'
# -*- coding: utf-8 -*-

import wx


class insertFrame(wx.Frame):
    def __init__(self, parent, id):
        wx.Frame.__init__(self, parent, id, "Frame_With_Button", (10, 10),(500, 500))
        panel = wx.Panel(self)#创建画板
        button = wx.Button(panel, label="close", pos=(-1, -1), size=(90, 50))#将按钮添加到画板上
        self.Bind(wx.EVT_BUTTON, self.onButton, button)#绑定按钮事件
        self.Bind(wx.EVT_BUTTON, self.CloseWindowButton)#绑定关闭窗口事件

    def onButton(self, event):
        self.Close(True)

    def CloseWindowButton(self, event):
        self.Close(True)


if __name__ == "__main__":
    app = wx.App()
    frame = insertFrame(None, -1)
    frame.Show()
    app.MainLoop()


