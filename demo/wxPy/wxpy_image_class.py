__author__ = 'root'
# -*- coding: utf-8 -*-
import wx


class Frame(wx.Frame): #wx.Frame 子类创建了一个基础的窗体 设定窗体的大小，名称，
    def __init__(self, image, parent=None, id=-1, pos=wx.DefaultPosition, title='hello,wxpython'):#图像处理参数
        #显示图像
        temp = image.ConvertToBitmap()
        size = temp.GetWidth(), temp.GetHeight()#设置长度和宽度
        wx.Frame.__init__(self, parent, id, title, pos, size)
        self.bmp = wx.StaticBitmap(parent=self, bitmap=temp)


class App(wx.App):#创建一些元素  然后将这些元素组合到 已经创建好的窗体上
    def OnInit(self): #wx.App子类
        #图像处理
        image = wx.Image('../images/001.jpg', wx.BITMAP_TYPE_JPEG)
        self.frame = Frame(image)

        self.frame.Show()
        self.SetTopWindow(self.frame)
        return True


def main():
    app = App()
    app.MainLoop()


if __name__ == '__main__':
    main()