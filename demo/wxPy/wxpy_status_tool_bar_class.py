__author__ = 'Administrator'
# -*- coding: utf-8 -*-
import wx.py.images as images
import wx


class StatusToolBar(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, -1, "tool_windown", (-1, -1), (500, 500))
        panel = wx.Panel(self)
        panel.SetBackgroundColour("White")
        statusBar = self.CreateStatusBar()#创建状态栏
        toolBar = self.CreateToolBar()#创建工具栏
        toolBar.AddSimpleTool(wx.NewId(), images.getPyBitmap(), "new", "Long help for 'new'")#给工具栏添加工具
        toolBar.Realize()#准备显示工具栏
        menubar = wx.MenuBar()#创建菜单栏
        #创建2个菜单
        menu1 = wx.Menu()
        menubar.Append(menu1, "&File")
        menu2 = wx.Menu()
        #创建菜单项目
        menu2.Append(wx.NewId(), "&copy", "copy in status bar")
        menu2.Append(wx.NewId(), "C&ut", "")
        menu2.Append(wx.NewId(), "&paste", "")
        menu2.AppendSeparator()
        menu2.Append(wx.NewId(), "&OPtions", "Display Options")
        menubar.Append(menu2, "&Edite")
        self.SetMenuBar(menubar)


if __name__ == "__main__":
    app = wx.App()
    frame = StatusToolBar()
    frame.Show()
    app.MainLoop()